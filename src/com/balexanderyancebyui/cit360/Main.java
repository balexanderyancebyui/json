package com.balexanderyancebyui.cit360;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Main {

    public static void main(String[] args) {

        Car car = new Car("asdf431", "Pontiac", "Firebird", 1992, "Red");

        String carJSON = carToJSON(car);
        System.out.println(carJSON);

        Car secondCar = jsonToCar(carJSON);
        System.out.println(secondCar.toString());

    }

    public static String carToJSON(Car car) {

        ObjectMapper mapper = new ObjectMapper();

        String json = "";

        try {
            json = mapper.writeValueAsString(car);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return(json);
    }

    public static Car jsonToCar(String json) {

        ObjectMapper mapper = new ObjectMapper();

        Car car = null;

        try {
            car = mapper.readValue(json, Car.class);
        } catch (JsonProcessingException e){
            e.printStackTrace();
        }

        return car;
    }
}
